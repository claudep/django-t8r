==========
Translator
==========

django-t8r, aka translator, is an umpteenth trial to add translatability to
Django model fields.
The prerequisite was to build the most unobstrusive application as possible.
This means that no database migration should be required to add or remove a
field for translation, or to add a new language. This also mean that it has to
be able to add translatable fields for 3rd party application. And finally,
developers should be able to use the i18n tools they are currently familiar
with, that is the gettext API (e.g. `_(instance.field)` should provide the
localized content).

Quick start
-----------

1. Install the application (`pip install django-t8r`).

2. Add "translator" to your `INSTALLED_APPS` setting:

    INSTALLED_APPS = [
        ...
        'translator',
    ]

3. Choose the fields you want to enable translation for and register them
through the `TRANSLATABLE_FIELDS` setting:

    TRANSLATABLE_FIELDS = {
        'app1.model1': ['field1', 'field2'],
        ...
    }

4. Run `python manage.py migrate` to create the translator model tables.

5. Run `python manage.py update_trans` to collect translatable strings from the
registered database fields.
