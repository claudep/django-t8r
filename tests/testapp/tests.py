from django.core.management import call_command
from django.test import TestCase
from django.utils.translation import gettext as _, override

from translator.models import Translatable, Translation

from .models import Article


class TranslatorTests(TestCase):
    def test_simple_transation_workflow(self):
        art = Article.objects.create(title='Some test', content='Here\nare\nsome\nlines.')
        self.assertEqual(Translatable.objects.all().count(), 2)
        Translation.objects.create(
            origin=Translatable.objects.get(msgid=art.title),
            locale='fr',
            translated='Un certain test',
        )
        Translation.objects.create(
            origin=Translatable.objects.get(msgid=art.content),
            locale='fr',
            translated='Voici\nquelques\nlignes.',
        )
        with override('fr'):
            self.assertEqual(_(art.title), 'Un certain test')
            self.assertEqual(_(art.content), 'Voici\nquelques\nlignes.')

    def test_translation_cleaned(self):
        """
        The update_trans command should remove stale Translatable/Translation
        instances.
        """
        trans = Translatable.objects.create(msgid='foo', msghash='bar')
        Translation.objects.create(
            origin=trans,
            locale='fr',
            translated='toto',
        )
        call_command('update_trans', verbosity=0)
        self.assertEqual(Translatable.objects.all().count(), 0)
        self.assertEqual(Translation.objects.all().count(), 0)
