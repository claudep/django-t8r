from django.apps import AppConfig
from django.conf import settings
from django.db.models.signals import post_save
from django.db.utils import DatabaseError


class TranslatorConfig(AppConfig):
    name = 'translator'

    def ready(self):
        from .signals import post_save_callback
        post_save.connect(post_save_callback)
        
        # Load dynamic translations for all languages
        from .models import Translation
        try:
            # Test if app has been migrated yet
            Translation.objects.first()
        except DatabaseError:
            pass
        else:
            for locale, _ in settings.LANGUAGES:
                Translation.merge_translations(locale)
