import os
import shutil
import tempfile

from polib import POFile, POEntry

from django.db import models
from django.utils.text import Truncator
from django.utils.translation.trans_real import translation


class Translatable(models.Model):
    msgid = models.TextField()
    msghash = models.CharField(max_length=32, unique=True)

    def __str__(self):
        return Truncator(self.msgid).words(20, truncate='…')


class Translation(models.Model):
    origin = models.ForeignKey(Translatable, on_delete=models.CASCADE)
    locale = models.CharField(max_length=12)
    translated = models.TextField()
    fuzzy = models.ForeignKey(
        Translatable, blank=True, null=True, related_name='fuzzies', on_delete=models.CASCADE
    )

    class Meta:
        unique_together = ('origin', 'locale')

    def __str__(self):
        return '{locale}{fuz}: {trans}'.format(
            locale=self.locale,
            fuz='' if self.fuzzy_id is None else ' (fuzzy)',
            trans=Truncator(self.translated).words(20, truncate='…')
        )

    def save(self, **kwargs):
        self.merge_translations(self.locale, translations=[self])

    @classmethod
    def merge_translations(cls, locale, translations=None):
        """Produce a .po file for `locale` with all Translations."""
        # Could be run in an async thread
        po = POFile(encoding='utf-8')
        po.metadata = {
            'Language': locale,
            'MIME-Version': '1.0',
            'Content-Type': 'text/plain; charset=utf-8',
            'Content-Transfer-Encoding': '8bit',
        }
        if translations is None:
            # Merge the entire dynamic catalog
            for msgid, trans in Translation.objects.exclude(fuzzy=True).filter(locale=locale
                    ).values_list('origin__msgid', 'translated'):
                # write to po
                po.append(POEntry(msgid=msgid, msgstr=trans))
        else:
            for tr in translations:
                po.append(POEntry(msgid=tr.origin.msgid, msgstr=tr.translated))
        tempdir = tempfile.mkdtemp()
        podir = os.path.join(tempdir, locale, 'LC_MESSAGES')
        os.makedirs(podir)
        # Save and compile to .mo
        po.save_as_mofile(os.path.join(podir, 'django.mo'))
        # Get the Django translation instance and merge with our catalog
        tr = translation(locale)
        tr.merge(tr._new_gnu_trans(tempdir, use_null_fallback=False))
        shutil.rmtree(tempdir)
