from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name='Translatable',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('msgid', models.TextField()),
                ('msghash', models.CharField(max_length=32, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Translation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('locale', models.CharField(max_length=12)),
                ('translated', models.TextField()),
                ('fuzzy', models.ForeignKey(blank=True, null=True, on_delete=models.deletion.CASCADE, related_name='fuzzies', to='translator.Translatable')),
                ('origin', models.ForeignKey(on_delete=models.deletion.CASCADE, to='translator.Translatable')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='translation',
            unique_together=set([('origin', 'locale')]),
        ),
    ]
