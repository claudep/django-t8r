from hashlib import md5
from itertools import chain

from django.apps import apps
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from translator.models import Translatable, Translation


class Command(BaseCommand):
    help = (
        'Remove Translatable not matching any model strings and insert new strings '
        'which may have slipped through the signal/forms update system.'
    )

    def handle(self, *args, **options):
        try:
            trans_fields = settings.TRANSLATABLE_FIELDS
        except AttributeError:
            raise CommandError("No TRANSLATABLE_FIELDS setting found.")
        verbosity = options['verbosity']

        # Get a mapping `{hash: real string, …}` of all translatable strings in
        # current state of the database.
        hashes = {}
        for model_key, fields in trans_fields.items():
            app_name, model_name = model_key.split('.')
            model = apps.get_app_config(app_name).get_model(model_name)
            strings = model._default_manager.all().values_list(*fields)
            hashes.update({
                md5(s.encode('utf-8')).hexdigest: s for s in set(chain.from_iterable(strings))
            })
        # Delete 'stale' strings
        to_be_deleted = Translatable.objects.exclude(msghash__in=hashes.keys())
        if verbosity > 0 and to_be_deleted.count() > 0:
            self.stdout.write('%d obsolete translatable string(s) deleted' %  to_be_deleted.count())
        to_be_deleted.delete()
        # Insert missing strings
        missing_hashes = hashes.keys() - set(Translatable.objects.values_list('msghash', flat=True))
        if missing_hashes:
            Translatable.objects.bulk_create([
                Translatable(msgid=hashes[_hash], msghash=_hash) for _hash in missing_hashes
            ])
        if verbosity > 0:
            self.stdout.write('%d new translatable string(s) created' % len(missing_hashes))
            
