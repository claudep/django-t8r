from django.contrib import admin

from .models import Translatable, Translation


@admin.register(Translatable)
class TranslatableAdmin(admin.ModelAdmin):
    list_display = ['__str__']


@admin.register(Translation)
class TranslationAdmin(admin.ModelAdmin):
    list_display = ['__str__']
