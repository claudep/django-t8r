from hashlib import md5
from django.conf import settings
from django.db import IntegrityError, transaction

from .models import Translatable


def post_save_callback(sender, instance, created=False, update_fields=None, **kwargs):
    # Check if any model field is registered as translatable
    model_key = '.'.join([instance._meta.app_label, instance._meta.model_name])
    if not model_key in settings.TRANSLATABLE_FIELDS:
        return
    target_fields = settings.TRANSLATABLE_FIELDS[model_key]
    for field_name in target_fields:
        content = getattr(instance, field_name)
        if content:
            try:
                with transaction.atomic():
                    Translatable.objects.create(msghash=md5(content.encode('utf-8')).hexdigest(), msgid=content)
            except IntegrityError:
                pass 
